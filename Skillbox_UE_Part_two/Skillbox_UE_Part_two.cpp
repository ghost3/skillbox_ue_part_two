﻿#include <iostream>
#include <vector>

class ConstForwardIterator {
private:
    // Убираем громоздкий тип данных итератора под using 
    // Чтобы мне не писать шаблон этот константный форвард-итератор для простоты работает только с int
    using IteratorType = std::vector<int>::const_iterator;

    //Сам итератор, член класса
    IteratorType iterator;

public:
    //Конструктор, принимающий ссылку на итератор вектора и записывающий в член класса
    ConstForwardIterator(const IteratorType& it)
    {
        iterator = it;
    }

    // Перегрузка операторов для forward-итератора
    // ++ префиксный
    ConstForwardIterator operator++() {
        ++iterator;
        return *this;
    }
    // ++ постфиксный
    ConstForwardIterator operator++(int) {
        ConstForwardIterator temp = *this;
        ++iterator;
        return temp;
    }

    // Логическое И
    bool operator==(const ConstForwardIterator& other) const {
        return iterator == other.iterator;
    }

    // Логическое НЕ
    bool operator!=(const ConstForwardIterator& other) const {
        return iterator != other.iterator;
    }

    // Возвращение значения по итреатору
    int operator*() {
        return *iterator;
    }
};

int main() {
    //Создаём массив и заполняем его
    std::vector<int> myVector = { 1, 2, 3, 4, 5 };

    // Использование константного forward-итератора
    ConstForwardIterator begin(myVector.begin());
    ConstForwardIterator end(myVector.end());

    //Пробегаемся итератором и выводим значения
    for (ConstForwardIterator it = begin; it != end; it++) {
        // Печатаем значение
        std::cout << *it << " ";
    }

    return 0;
}





